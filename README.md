# Stability Sort

This project requires the following commands be in the PATH:

- [`bcftools`](https://github.com/samtools/bcftools)
- [`bgzip`](https://github.com/samtools/htslib)
- [`conda`](https://docs.conda.io/projects/miniconda/en/latest)

Conda must have an environment called "pymol" that contains PyMol (>= v2.5). Please run:

```bash
conda create -n pymol -y
conda activate pymol
conda install -c conda-forge -c schrodinger pymol-bundle -y
```

Or follow [these instructions](https://pymol.org/conda).
