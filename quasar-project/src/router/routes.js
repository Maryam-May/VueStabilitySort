import ProteinDisplay from '../pages/ProteinDisplay.vue';
import VariantsListDisplay from '../pages/VariantsListDisplay.vue';

const routes = [
  {
    path: '/',
    component: () => import('../layouts/MainLayout.vue'),
    children: [
      {
        path: '',
        name: 'Home',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import('../pages/IndexPage.vue'),
        // meta: {
        //   title: 
        // }
      },
      {
        path: '/analyse/:hash',
        name: 'analyse',
        component: VariantsListDisplay
      },
      {
        path: '/protein/:uniprot',
        name: 'protein',
        component: ProteinDisplay,
      }
    ]
  },
  {
    path: '/download',
    component: () => import('pages/DownloadData.vue')
  },
  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  }
]

export default routes;