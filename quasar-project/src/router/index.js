import { route } from 'quasar/wrappers'
import { createRouter, createMemoryHistory, createWebHistory, createWebHashHistory } from 'vue-router'
import routes from './routes'

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

export default route(function (/* { store, ssrContext } */) {
  const createHistory = process.env.SERVER
    ? createMemoryHistory
    : (process.env.VUE_ROUTER_MODE === 'history' ? createWebHistory : createWebHashHistory)

  const Router = createRouter({
    scrollBehavior: () => ({ left: 0, top: 0 }),
    routes,

    // Leave this as is and make changes in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    history: createHistory(process.env.VUE_ROUTER_BASE)
  })

  // Router.beforeEach((to, from) => {
  //   // https://www.digitalocean.com/community/tutorials/vuejs-vue-router-modify-head

  //   // This goes through the matched routes from last to first, finding the closest route with a title.
  //   // e.g., if we have `/some/deep/nested/route` and `/some`, `/deep`, and `/nested` have titles,
  //   // `/nested`'s will be chosen.
  //   const nearestWithTitle = to.matched.slice().reverse().find(r => r.meta && r.meta.title);

  //   const previousNearestWithTitle = from.matched.slice().reverse().find(r => r.meta && r.meta.title);

  //   // If a route with a title was found, set the document (page) title to that value.
  //   if(nearestWithTitle) {
  //     document.title = nearestWithTitle.meta.title;
  //   } else if(previousNearestWithTitle) {
  //     document.title = previousNearestWithTitle.meta.title;
  //   }
  // });

  return Router
})
