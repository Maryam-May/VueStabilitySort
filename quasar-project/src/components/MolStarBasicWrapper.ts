/**
 * Copyright (c) 2019 mol* contributors, licensed under MIT, See LICENSE file for more info.
 *
 * @author David Sehnal <david.sehnal@gmail.com>
 */

import { PDBeStructureQualityReport } from 'molstar/lib/extensions/pdbe';
import { EmptyLoci } from 'molstar/lib/mol-model/loci';
import { Structure, StructureSelection } from 'molstar/lib/mol-model/structure';
import { AnimateModelIndex } from 'molstar/lib/mol-plugin-state/animation/built-in/model-index';
import { BuiltInTrajectoryFormat } from 'molstar/lib/mol-plugin-state/formats/trajectory';
import { createPluginUI } from 'molstar/lib/mol-plugin-ui/react18';
import { PluginUIContext } from 'molstar/lib/mol-plugin-ui/context';
import { DefaultPluginUISpec } from 'molstar/lib/mol-plugin-ui/spec';
import { PluginCommands } from 'molstar/lib/mol-plugin/commands';
import { Script } from 'molstar/lib/mol-script/script';
import { Asset } from 'molstar/lib/mol-util/assets';
import { Color } from 'molstar/lib/mol-util/color';
import { StripedResidues } from './MolStarBasicWrapperColoring';
// import { CustomToastMessage } from './MolStarBasicWrapperControls';
import { CustomColorThemeProvider } from './MolStarBasicWrapperCustomTheme';
// import './index.html';
import { buildStaticSuperposition, dynamicSuperpositionTest, StaticSuperpositionTestData } from './MolStarBasicWrapperSuperposition';
import { StructureSelectionQuery } from 'molstar/lib/mol-plugin-state/helpers/structure-selection-query';
import { plugins } from 'app/postcss.config.cjs';
import { ParamDefinition } from 'molstar/lib/mol-util/param-definition';
import { Canvas3D, Canvas3DParams } from 'molstar/lib/mol-canvas3d/canvas3d';
import { Camera } from 'molstar/lib/mol-canvas3d/camera';
// require('mol-plugin-ui/skin/light.scss');
// import.meta.url('mol-plugin-ui/skin/light.scss');
import { Observable, Subscription } from 'rxjs';

type LoadParams = { input: string, format?: BuiltInTrajectoryFormat, isBinary?: boolean, assemblyId?: string }

export default class BasicWrapper {
    plugin: PluginUIContext;

    async init(target: string | HTMLElement) {

        this.plugin = await createPluginUI(typeof target === 'string' ? document.getElementById(target)! : target, {
            ...DefaultPluginUISpec(),
            layout: {
                initial: {
                    isExpanded: false,
                    showControls: false
                }
            },
            components: {
                remoteState: "default"
            },
            canvas3d: {
                cameraFog: { name: "off", params: {} },
                cameraClipping: {"far": false}
            }
        });

        // this.plugin = await createPluginUI(typeof target === 'string' ? document.getElementById(target)! : target);

        // (this.plugin.customState as any) = {
        //     colorPalette: {
        //         name: 'colors',
        //         params: { list: { colors } }
        //     }
        // };

        PluginCommands.Canvas3D.SetSettings(this.plugin, { settings: props => {
            props.renderer.selectColor = Color(0xff0000);
            props.marking.selectEdgeColor = Color(0xff0000);
            // props.renderer.selectStrength = 100;
            // props.renderer.dimColor = Color(0xff0000);
            // props.renderer.ambientColor = Color(0xff0000);
        } });

        this.plugin.representation.structure.themes.colorThemeRegistry.add(StripedResidues.colorThemeProvider!);
        this.plugin.representation.structure.themes.colorThemeRegistry.add(CustomColorThemeProvider);
        this.plugin.managers.lociLabels.addProvider(StripedResidues.labelProvider!);
        this.plugin.customModelProperties.register(StripedResidues.propertyProvider, true);
        // this.plugin.canvas3d?.didDraw.
        // Subscription/
        
        // console.log(this.plugin.canvas3d?.d);
        // xs.subscribe(f)
    }

    async load(target: string | HTMLElement, { input, format = 'mmcif', isBinary = false, assemblyId = '' }: LoadParams) {
        await this.plugin.clear();

        // this.plugin.unmount();
        // this.plugin.mount(typeof target === 'string' ? document.getElementById(target)! : target);

        // const data = await this.plugin.builders.data.download({ url: Asset.Url(url), isBinary }, { state: { isGhost: true } });
        const dataLabel = Math.random().toString().substring(2,5);

        const data = await this.plugin.builders.data.rawData({ data: input }, { ref: dataLabel, tags: dataLabel});
        // const data = await this.plugin.builders.data.readFile({ file: Asset.File(fileBuffer), isBinary }, { state: { isGhost: true } });
        const trajectory = await this.plugin.builders.structure.parseTrajectory(data, format);

        await this.plugin.builders.structure.hierarchy.applyPreset(trajectory, 'default', {
            structure: assemblyId ? {
                name: 'assembly',
                params: { id: assemblyId }
            } : {
                name: 'model',
                params: {}
            },
            showUnitcell: false,
            representationPreset: 'auto'
        });

        // this.plugin.build().delete(trajectory);
        // this.plugin.build().commit();
        // this.plugin.build().getTree()
    }

    setBackground(color: number) {
        PluginCommands.Canvas3D.SetSettings(this.plugin, { settings: props => { props.renderer.backgroundColor = Color(color); } });
    }

    getProteinPosition() {
        return this.plugin.managers.structure.selection.entries;
    }

    subscribeToPluginUpdates(pl: PluginUIContext) {
        // pl.canvas3d?.didDraw.subscribe(x => console.log(`from other context: ${x}`));
        const subscription = pl.canvas3d?.didDraw.subscribe(() => this.restoreCameraState(pl.canvas3d.camera.state));

        // const selfSubscription = this.plugin.canvas3d?.didDraw.subscribe(() => this.restoreCameraState(pl.canvas3d.camera.state));
        // subscription.add(selfSubscription);

        return subscription;
    }

    // subscribeToUpdates() {
    //     this.plugin.canvas3d?.didDraw.subscribe(x => console.log(x));
    //     this.plugin.canvas3d?.didDraw.subscribe(() => console.log(this.plugin.canvas3d.camera.getSnapshot()));
    //     return this.plugin.canvas3d?.didDraw.subscribe(() => this.captureCameraState(this.plugin));
    // }

    unsubscribeFromPluginUpdates(subscription: Subscription) {
        subscription.unsubscribe();
    }

    // captureCameraState(pl: PluginUIContext) {
    //     return pl.canvas3d.camera.state;
    // }

    // constructor() {
    //     this.plugin.
    // }

    restoreCameraState(state: Partial<Camera.Snapshot>) {
        console.log(state);
        this.plugin.canvas3d.camera.setState(state);
    }

    async takePicture(width: number, height: number) {
        // this.plugin.canvas3d.camera.
        // await this.plugin.saveImage(path.join(args.outDirectory, 'basic.png'));
        // await this.plugin.saveImage(path.join(args.outDirectory, 'basic.jpg'));
        // await this.plugin.saveImage(path.join(args.outDirectory, 'large.png'), { width: 1600, height: 1200 });
        // // this.plugin.vi
        // // this.plugin.managers.snapshot.

        // const renderer = this.plugin.canvas3d!.props.renderer;
        // PluginCommands.Canvas3D.SetSettings(this.plugin, { settings: { renderer: { ...renderer, highlightColor: 0xff0000 as Color} } });
        // this.plugin.helpers.viewportScreenshot.behaviors.values.value.axes.

        // const viewportValues = this.plugin.helpers.viewportScreenshot.behaviors.values;
        // const resizedViewportData = {
        //     ...this.plugin.helpers.viewportScreenshot!.behaviors.values.value,
        //     resolution: { name: "custom" as const, params: { width: width, height: height } }
        //     // resolution: { name: "viewport" }
        // };

        const pl = this.plugin;
        pl.helpers.viewportScreenshot!.behaviors.values.next({
            ...pl.helpers.viewportScreenshot!.behaviors.values.value,
            resolution: { name: "custom", params: { width, height } }
            // resolution: { name: "viewport" }
        });

        // pl.helpers.viewportScreenshot!.download("testImage.png");
        // pl.helpers.viewportScreenshot!.

        console.log(pl.helpers.viewportScreenshot);
        // await this.plugin.helpers.viewportScreenshot!.copyToClipboard();
        const imageDataUri = await pl.helpers.viewportScreenshot!.getImageDataUri();
        // const imageDataUri = pl.helpers.viewportScreenshot!.getFilename();
        return imageDataUri;
    }

    toggleSpin() {
        if (!this.plugin.canvas3d) return;

        const trackball = this.plugin.canvas3d.props.trackball;
        PluginCommands.Canvas3D.SetSettings(this.plugin, {
            settings: {
                trackball: {
                    ...trackball,
                    animate: trackball.animate.name === 'spin'
                        ? { name: 'off', params: {} }
                        : { name: 'spin', params: { speed: 1 } }
                }
            }
        });
        if (this.plugin.canvas3d.props.trackball.animate.name !== 'spin') {
            PluginCommands.Camera.Reset(this.plugin, {});
        }
    }

    private animateModelIndexTargetFps() {
        return Math.max(1, this.animate.modelIndex.targetFps | 0);
    }

    animate = {
        modelIndex: {
            targetFps: 8,
            onceForward: () => { this.plugin.managers.animation.play(AnimateModelIndex, { duration: { name: 'computed', params: { targetFps: this.animateModelIndexTargetFps() } }, mode: { name: 'once', params: { direction: 'forward' } } }); },
            onceBackward: () => { this.plugin.managers.animation.play(AnimateModelIndex, { duration: { name: 'computed', params: { targetFps: this.animateModelIndexTargetFps() } }, mode: { name: 'once', params: { direction: 'backward' } } }); },
            palindrome: () => { this.plugin.managers.animation.play(AnimateModelIndex, { duration: { name: 'computed', params: { targetFps: this.animateModelIndexTargetFps() } }, mode: { name: 'palindrome', params: {} } }); },
            loop: () => { this.plugin.managers.animation.play(AnimateModelIndex, { duration: { name: 'computed', params: { targetFps: this.animateModelIndexTargetFps() } }, mode: { name: 'loop', params: { direction: 'forward' } } }); },
            stop: () => this.plugin.managers.animation.stop()
        }
    };

    coloring = {
        applyStripes: async () => {
            this.plugin.dataTransaction(async () => {
                for (const s of this.plugin.managers.structure.hierarchy.current.structures) {
                    await this.plugin.managers.structure.component.updateRepresentationsTheme(s.components, { color: StripedResidues.propertyProvider.descriptor.name as any });
                }
            });
        },
        applyCustomTheme: async () => {
            this.plugin.dataTransaction(async () => {
                for (const s of this.plugin.managers.structure.hierarchy.current.structures) {
                    await this.plugin.managers.structure.component.updateRepresentationsTheme(s.components, { color: CustomColorThemeProvider.name as any });
                }
            });
        },
        applyDefault: async () => {
            this.plugin.dataTransaction(async () => {
                for (const s of this.plugin.managers.structure.hierarchy.current.structures) {
                    await this.plugin.managers.structure.component.updateRepresentationsTheme(s.components, { color: 'default' });
                }
            });
        }
    };

    interactivity = {
        highlightOn: (seqID: number) => {
            const data = this.plugin.managers.structure.hierarchy.current.structures[0]?.cell.obj?.data;
            if (!data) return;

            const sel = Script.getStructureSelection(Q => Q.struct.generator.atomGroups({
                'residue-test': Q.core.rel.eq([Q.struct.atomProperty.macromolecular.label_seq_id(), seqID]),
                'group-by': Q.struct.atomProperty.macromolecular.residueKey()
            }), data);

            const loci = StructureSelection.toLociWithSourceUnits(sel);
            // this.plugin.managers.interactivity.lociHighlights.highlightOnly({ loci });
            this.plugin.managers.interactivity.lociSelects.selectOnly({ loci });
            this.plugin.managers.camera.focusLoci(loci);
            this.plugin.managers.structure.focus.setFromLoci(loci);
        },
        clearHighlight: () => {
            this.plugin.managers.interactivity.lociHighlights.highlightOnly({ loci: EmptyLoci });
        }
    };

    tests = {
        staticSuperposition: async () => {
            await this.plugin.clear();
            return buildStaticSuperposition(this.plugin, StaticSuperpositionTestData);
        },
        dynamicSuperposition: async () => {
            await this.plugin.clear();
            return dynamicSuperpositionTest(this.plugin, ['1tqn', '2hhb', '4hhb'], 'HEM');
        },
        toggleValidationTooltip: () => {
            return this.plugin.state.updateBehavior(PDBeStructureQualityReport, params => { params.showTooltip = !params.showTooltip; });
        },
        showToasts: () => {
            PluginCommands.Toast.Show(this.plugin, {
                title: 'Toast 1',
                message: 'This is an example text, timeout 3s',
                key: 'toast-1',
                timeoutMs: 3000
            });
            PluginCommands.Toast.Show(this.plugin, {
                title: 'Toast 2',
                message: "hey bud",
                key: 'toast-2'
            });
        },
        hideToasts: () => {
            PluginCommands.Toast.Hide(this.plugin, { key: 'toast-1' });
            PluginCommands.Toast.Hide(this.plugin, { key: 'toast-2' });
        }
    };
}
