/**
 * Copyright (c) 2019 mol* contributors, licensed under MIT, See LICENSE file for more info.
 *
 * @author David Sehnal <david.sehnal@gmail.com>
 */

import { PDBeStructureQualityReport } from 'molstar/lib/extensions/pdbe';
import { EmptyLoci } from 'molstar/lib/mol-model/loci';
import { Structure, StructureSelection } from 'molstar/lib/mol-model/structure';
import { AnimateModelIndex } from 'molstar/lib/mol-plugin-state/animation/built-in/model-index';
import { BuiltInTrajectoryFormat } from 'molstar/lib/mol-plugin-state/formats/trajectory';
import { createPluginUI } from 'molstar/lib/mol-plugin-ui/react18';
import { PluginUIContext } from 'molstar/lib/mol-plugin-ui/context';
import { DefaultPluginUISpec } from 'molstar/lib/mol-plugin-ui/spec';
import { PluginCommands } from 'molstar/lib/mol-plugin/commands';
import { Script } from 'molstar/lib/mol-script/script';
import { Asset } from 'molstar/lib/mol-util/assets';
import { Color } from 'molstar/lib/mol-util/color';
import { StripedResidues } from './MolStarBasicWrapperColoring';
// import { CustomToastMessage } from './MolStarBasicWrapperControls';
import { CustomColorThemeProvider } from './MolStarBasicWrapperCustomTheme';
// import './index.html';
import { buildStaticSuperposition, dynamicSuperpositionTest, StaticSuperpositionTestData } from './MolStarBasicWrapperSuperposition';
import { StructureSelectionQuery } from 'molstar/lib/mol-plugin-state/helpers/structure-selection-query';
import { plugins } from 'app/postcss.config.cjs';
import { ParamDefinition } from 'molstar/lib/mol-util/param-definition';
import { Canvas3D, Canvas3DParams } from 'molstar/lib/mol-canvas3d/canvas3d';
// require('mol-plugin-ui/skin/light.scss');
// import.meta.url('mol-plugin-ui/skin/light.scss');

type LoadParams = { input: string, format?: BuiltInTrajectoryFormat, isBinary?: boolean, assemblyId?: string }

class BasicWrapper {
    plugin: PluginUIContext;

    async init(target: string | HTMLElement) {
        // this.plugin = await createPluginUI(typeof target === 'string' ? document.getElementById(target)! : target, {
        //     ...DefaultPluginUISpec(),
        //     layout: {
        //         initial: {
        //             isExpanded: false,
        //             showControls: false
        //         }
        //     },
        //     components: {
        //         remoteState: "none"
        //     },
        //     canvas3d: {
        //         cameraFog: { name: "off", params: {} },
        //         cameraClipping: {"far": false}
        //     }
        // });

        this.plugin = await createPluginUI(typeof target === 'string' ? document.getElementById(target)! : target);


        console.log(this.plugin);

        this.plugin.representation.structure.themes.colorThemeRegistry.add(StripedResidues.colorThemeProvider!);
        this.plugin.representation.structure.themes.colorThemeRegistry.add(CustomColorThemeProvider);
        this.plugin.managers.lociLabels.addProvider(StripedResidues.labelProvider!);
        this.plugin.customModelProperties.register(StripedResidues.propertyProvider, true);

        return this;
    }

    async load(target: string | HTMLElement, { input, format = 'mmcif', isBinary = false, assemblyId = '' }: LoadParams) {
        console.log("fine");
        await this.plugin.clear();

        this.plugin.unmount();
        this.plugin.mount(typeof target === 'string' ? document.getElementById(target)! : target);

        // const data = await this.plugin.builders.data.download({ url: Asset.Url(url), isBinary }, { state: { isGhost: true } });
        const dataLabel = Math.random().toString().substring(2,5);

        console.log("fine");
        const data = await this.plugin.builders.data.rawData({ data: input }, { ref: dataLabel, tags: dataLabel});
        // const data = await this.plugin.builders.data.readFile({ file: Asset.File(fileBuffer), isBinary }, { state: { isGhost: true } });
        console.log("fine");
        const trajectory = await this.plugin.builders.structure.parseTrajectory(data, format);

        console.log("fine");
        console.log(this.plugin);
        console.log("fine");
        await this.plugin.builders.structure.hierarchy.applyPreset(trajectory, 'default', {
            structure: assemblyId ? {
                name: 'assembly',
                params: { id: assemblyId }
            } : {
                name: 'model',
                params: {}
            },
            showUnitcell: false,
            representationPreset: 'auto'
        });

        // this.plugin.build().delete(trajectory);
        // this.plugin.build().commit();
        // this.plugin.build().getTree()
    }

    setBackground(color: number) {
        PluginCommands.Canvas3D.SetSettings(this.plugin, { settings: props => { props.renderer.backgroundColor = Color(color); } });
    }

    getProteinPosition() {
        return this.plugin.managers.structure.selection.entries;
    }

    toggleSpin() {
        if (!this.plugin.canvas3d) return;

        const trackball = this.plugin.canvas3d.props.trackball;
        PluginCommands.Canvas3D.SetSettings(this.plugin, {
            settings: {
                trackball: {
                    ...trackball,
                    animate: trackball.animate.name === 'spin'
                        ? { name: 'off', params: {} }
                        : { name: 'spin', params: { speed: 1 } }
                }
            }
        });
        if (this.plugin.canvas3d.props.trackball.animate.name !== 'spin') {
            PluginCommands.Camera.Reset(this.plugin, {});
        }
    }

    private animateModelIndexTargetFps() {
        return Math.max(1, this.animate.modelIndex.targetFps | 0);
    }

    animate = {
        modelIndex: {
            targetFps: 8,
            onceForward: () => { this.plugin.managers.animation.play(AnimateModelIndex, { duration: { name: 'computed', params: { targetFps: this.animateModelIndexTargetFps() } }, mode: { name: 'once', params: { direction: 'forward' } } }); },
            onceBackward: () => { this.plugin.managers.animation.play(AnimateModelIndex, { duration: { name: 'computed', params: { targetFps: this.animateModelIndexTargetFps() } }, mode: { name: 'once', params: { direction: 'backward' } } }); },
            palindrome: () => { this.plugin.managers.animation.play(AnimateModelIndex, { duration: { name: 'computed', params: { targetFps: this.animateModelIndexTargetFps() } }, mode: { name: 'palindrome', params: {} } }); },
            loop: () => { this.plugin.managers.animation.play(AnimateModelIndex, { duration: { name: 'computed', params: { targetFps: this.animateModelIndexTargetFps() } }, mode: { name: 'loop', params: { direction: 'forward' } } }); },
            stop: () => this.plugin.managers.animation.stop()
        }
    };

    coloring = {
        applyStripes: async () => {
            this.plugin.dataTransaction(async () => {
                for (const s of this.plugin.managers.structure.hierarchy.current.structures) {
                    await this.plugin.managers.structure.component.updateRepresentationsTheme(s.components, { color: StripedResidues.propertyProvider.descriptor.name as any });
                }
            });
        },
        applyCustomTheme: async () => {
            this.plugin.dataTransaction(async () => {
                for (const s of this.plugin.managers.structure.hierarchy.current.structures) {
                    await this.plugin.managers.structure.component.updateRepresentationsTheme(s.components, { color: CustomColorThemeProvider.name as any });
                }
            });
        },
        applyDefault: async () => {
            this.plugin.dataTransaction(async () => {
                for (const s of this.plugin.managers.structure.hierarchy.current.structures) {
                    await this.plugin.managers.structure.component.updateRepresentationsTheme(s.components, { color: 'default' });
                }
            });
        }
    };

    interactivity = {
        highlightOn: (seqID: number) => {
            const data = this.plugin.managers.structure.hierarchy.current.structures[0]?.cell.obj?.data;
            if (!data) return;

            // const seq_id = 7;
            const sel = Script.getStructureSelection(Q => Q.struct.generator.atomGroups({
                'residue-test': Q.core.rel.eq([Q.struct.atomProperty.macromolecular.label_seq_id(), seqID]),
                'group-by': Q.struct.atomProperty.macromolecular.residueKey()
            }), data);
            console.log(sel);
            const loci = StructureSelection.toLociWithSourceUnits(sel);
            console.log(loci);
            // this.plugin.managers.interactivity.lociHighlights.highlightOnly({ loci });
            this.plugin.managers.interactivity.lociSelects.selectOnly({ loci });
            this.plugin.managers.camera.focusLoci(loci);
            this.plugin.managers.structure.focus.setFromLoci(loci);
            console.log(this.plugin.managers.structure.selection.entries);
        },
        clearHighlight: () => {
            this.plugin.managers.interactivity.lociHighlights.highlightOnly({ loci: EmptyLoci });
        }
    };

    tests = {
        staticSuperposition: async () => {
            await this.plugin.clear();
            return buildStaticSuperposition(this.plugin, StaticSuperpositionTestData);
        },
        dynamicSuperposition: async () => {
            await this.plugin.clear();
            return dynamicSuperpositionTest(this.plugin, ['1tqn', '2hhb', '4hhb'], 'HEM');
        },
        toggleValidationTooltip: () => {
            return this.plugin.state.updateBehavior(PDBeStructureQualityReport, params => { params.showTooltip = !params.showTooltip; });
        },
        showToasts: () => {
            PluginCommands.Toast.Show(this.plugin, {
                title: 'Toast 1',
                message: 'This is an example text, timeout 3s',
                key: 'toast-1',
                timeoutMs: 3000
            });
            PluginCommands.Toast.Show(this.plugin, {
                title: 'Toast 2',
                message: "hey bud",
                key: 'toast-2'
            });
        },
        hideToasts: () => {
            PluginCommands.Toast.Hide(this.plugin, { key: 'toast-1' });
            PluginCommands.Toast.Hide(this.plugin, { key: 'toast-2' });
        }
    };
}

// (window as any).BasicMolStarWrapper = new BasicWrapper();
export default new BasicWrapper();
