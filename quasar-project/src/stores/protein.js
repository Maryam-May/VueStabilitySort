import { defineStore } from "pinia";
import { ref, reactive, computed, watch } from "vue";
import VariantDataService from "../../services/VariantDataService";

export const useProteinStore = defineStore('protein', () => {
  const uniprotID = ref("");
  const wildtypeSequence = ref("");

  /** @type {{ aa_pos: number, aa_alt: string }[]} */
  const gnomadValues = ref([]);

  /** @type {{ aa_pos: number, aa_alt: string, aa_ref: string | null }[]} */
  const mutations = ref([]);

  const activeMutation = computed(() => mutations.value[0] != undefined ? mutations.value[0] : {aa_pos: null, aa_alt: null, aa_ref: null});

  function updateWtSequence() {
    VariantDataService.getWtSequence(uniprotID)
      .then(response => {
        wildtypeSequence.value = response.data[0].sequence;
      });
  }

  watch(uniprotID, (newValue, oldValue) => {
    if (oldValue != newValue && oldValue != null && oldValue != "") {
      updateWtSequence()
    }
  })

  function updateAARef(i = 0) {
    mutations.value[i].aa_ref = wildtypeSequence[mutations.value[i].aa_pos - 1]
  }

  // function getAARef(aa_pos) {
  //   return wildtypeSequence[aa_pos - 1];
  // }

  async function setActiveMutation({aa_pos = null, aa_alt = null}) {
    mutations.value.unshift({
      aa_pos: aa_pos,
      aa_alt: aa_alt,
      aa_ref: wildtypeSequence[aa_pos - 1]
    })

    mutations.value.splice(1, 1);
  }

  const wtViewport = ref({});

  // watch()

  return { uniprotID, wildtypeSequence, gnomadValues, activeMutation, updateAARef, wtViewport, setActiveMutation }
})
