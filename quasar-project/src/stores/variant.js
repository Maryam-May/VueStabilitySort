import { defineStore } from "pinia";
import { ref, reactive, computed, watch } from "vue";
import VariantDataService from "../../services/VariantDataService";

export const useVariantStore = defineStore('variant', () => {
  const hash = ref("");
  const hashReady = ref(false);

  async function checkHashReady(intervalToClear = "") {
    console.log(hash.value);
    const hashReadyRes = await VariantDataService.checkHash(hash.value);
    console.log(hashReadyRes.status);
    console.log(hashReadyRes.data);

    if (hashReadyRes.status == 200 && hashReadyRes.data.endsWith("Complete!")) {
      if (intervalToClear != "") {
        clearInterval(intervalToClear);
      }
      hashReady.value = true;
    } else {
      hashReady.value = false;
    }
  }

  /** @type {{ chr: string, pos: number, alt: string, ref: string | null, phase: string | null, uniprot: string | null, enst: string | null, aa_pos: number | null, aa_ref: string | null, aa_alt: string | null, ddg: number | null, z: number | null }[]} */
  const variants = ref([])

  const activeVariant = computed(() => variants.value[0] != undefined ? variants.value[0] : {chr: null, pos: null, alt: null})

  const allVariants = ref([]); // populated by VariantsGnomadTable.vue

  const essentialValues = ["chr", "pos", "alt", "ref", "phase", "uniprot", "enst", "aa_pos", "aa_ref", "aa_alt", "ddg", "score"];

  async function refreshActiveVariant({ _hash = hash.value, chr, pos, alt, ref = null, phase = null, uniprot = null, enst = null, aa_pos = null, aa_ref = null, aa_alt = null, ddg = null, score = null }) {
    // variants.value.shift();

    const variantParams = ["chr", "pos", "alt", "ref", "phase", "uniprot", "enst", "aa_pos", "aa_ref", "aa_alt", "ddg", "score"];

    console.log( _hash, chr, pos, alt);
    console.log(activeVariant.value);
    console.log(variants.value);

    variants.value.unshift({
      chr: chr,
      pos: pos,
      alt: alt,
      ref: ref,
      phase: phase,
      uniprot: uniprot,
      enst: enst,
      aa_pos: aa_pos,
      aa_ref: aa_ref,
      aa_alt: aa_alt,
      ddg: ddg,
      score: score,
    })

    // const response = await VariantDataService.retrieveVariantInfo(_hash, activeVariant.value ? Object.keys(activeVariant.value).join(",") : variantParams.join(","), chr, pos, alt);
    // console.log(response);

    // response.data[0].uniprot = response.data[0].uniprot.split("-")[0].trim();

    // variants.value.unshift(variantParams.reduce((result, key) => {
    //   if (response.data[0].hasOwnProperty(key)) {
    //     result[key] = response.data[0][key];
    //   }
    //   return result;
    // }, {}));
    variants.value.splice(1, 1);
    console.log(activeVariant.value);
    console.log(variants.value);
    // activeVariant.value = response.data[0];
  }

  return { hash, hashReady, checkHashReady, activeVariant, refreshActiveVariant, essentialValues }
})
