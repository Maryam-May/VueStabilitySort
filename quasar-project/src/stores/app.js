import { defineStore } from "pinia";
import { ref } from "vue";
import VariantDataService from "../../services/VariantDataService";

export const useAppStore = defineStore('app', () => {
  const uniprotMap = ref(null);

  async function refreshUniprotMap() {
    VariantDataService.getUniprotMap()
    .then(response => {
      const groupedMap = new Map();
      for (const e of response.data) {
          if (!groupedMap.has(e.uniprot)) {
              groupedMap.set(e.uniprot, []);
          }
          groupedMap.get(e.uniprot).push(e.SYMBOL);
      }

      uniprotMap.value = groupedMap;
    });
  }

  const localColours = ref(true);

  return { uniprotMap, localColours, refreshUniprotMap }
})
