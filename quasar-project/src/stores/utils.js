import { defineStore } from "pinia";
import { ref, reactive, computed } from "vue";
import { useAppStore } from "stores/app.js";

export const useUtilsStore = defineStore('utils', () => {

  const colourMapRaw = reactive({
    ddg: ['#67001f','#b2182b','#d6604d','#f4a582','#fddbc7','#f7f7f7','#d1e5f0','#92c5de','#4393c3','#2166ac','#053061'], // colourbrewer2 11-class RdBu
    delta_score: ['#8e0152','#c51b7d','#de77ae','#f1b6da','#fde0ef','#f7f7f7','#e6f5d0','#b8e186','#7fbc41','#4d9221','#276419'], // colourbrewer2 11-class PiYG
    score: ['#7f3b08','#b35806','#e08214','#fdb863','#fee0b6','#f7f7f7','#d8daeb','#b2abd2','#8073ac','#542788','#2d004b'], // colourbrewer2 11-class PuOr
  })

  const calibratedEndsDefaults = {
    ddg: [-5, 5],
    delta_score: [-50, 50],
    score: [-25, 25]
  };

  const calibratedEnds = reactive(calibratedEndsDefaults);

  // const calibratedEndsDefaults = ref({
  //   ddg: [],
  //   delta_score: [],
  //   score: []
  // })

  const objectMap = (obj, fn) =>
  Object.fromEntries(
    Object.entries(obj).map(
      ([k, v], i) => [k, fn(v, k, i)]
    )
  )

  const colourMap = computed(() => {
    if (useAppStore().localColours) {
      return objectMap(colourMapRaw, (v,k,i) => {console.log("checking colours", k, calibratedEnds[k], v); return selectColours(calibratedEnds[k], v)});
    } else {
      return objectMap(colourMapRaw, (v,k,i) => {console.log("checking colours", k, calibratedEnds[k], v); return selectColours(calibratedEnds[k], v, calibratedEnds[k][1] / ((v.length - 1) / 2), (v.length - 1) / 2)});
    }
    // var newObj = {};
    // Object.keys(colourMapRaw).forEach((key) => {
    //   console.log("checking colours", key, calibratedEnds[key], colourMapRaw[key]);
    //   newObj[key] = selectColours(calibratedEnds[key], colourMapRaw[key]);
    //   // if (calibratedEnds == []) {
    //   //   return selectColours([rangeMin, rangeMax], colourMapRaw[key]);
    //   // } else {
    //   //   return selectColours([rangeMin, rangeMax], colourMapRaw[key]);
    //   // }
      
    //   // return  ? colourMapRaw[key] : colourMapRaw[key].reverse()
    // })
    // return newObj;
    // // return {
    // //   ddg: ['#67001f','#b2182b','#d6604d','#f4a582','#fddbc7','#f7f7f7','#d1e5f0','#92c5de','#4393c3','#2166ac','#053061'], // colourbrewer2 11-class RdBu
    // //   delta_score: ['#8e0152','#c51b7d','#de77ae','#f1b6da','#fde0ef','#f7f7f7','#e6f5d0','#b8e186','#7fbc41','#4d9221','#276419'], // colourbrewer2 11-class PiYG
    // //   score: ['#7f3b08','#b35806','#e08214','#fdb863','#fee0b6','#f7f7f7','#d8daeb','#b2abd2','#8073ac','#542788','#2d004b'], // colourbrewer2 11-class PuOr  
    // // }
  })

  // const ddgColourMap = ref(['#67001f','#b2182b','#d6604d','#f4a582','#fddbc7','#f7f7f7','#d1e5f0','#92c5de','#4393c3','#2166ac','#053061']); // colourbrewer2 11-class RdBu

  // const delta_scoreColourMap = ref(['#8e0152','#c51b7d','#de77ae','#f1b6da','#fde0ef','#f7f7f7','#e6f5d0','#b8e186','#7fbc41','#4d9221','#276419']); // colourbrewer2 11-class PiYG

  // const scoreColourMap = ref(['#7f3b08','#b35806','#e08214','#fdb863','#fee0b6','#f7f7f7','#d8daeb','#b2abd2','#8073ac','#542788','#2d004b']); // colourbrewer2 11-class PuOr

  // function calibrateEndsOld(rangeMin, rangeMax, step = 1, colourMin = -5, colourMax = 5) {
  //   rangeMin = Math.max(Math.floor(rangeMin / step) * step, colourMin);
  //   rangeMax = Math.min(Math.ceil(rangeMax / step) * step, colourMax);

  //   return [rangeMin, rangeMax];
  // }

  // function selectColoursOld([rangeMin, rangeMax], colourRange, centerIndex = 5) {
  //   // rangeMin = Math.floor(rangeMin / 0.5) / 0.5; // round to 0.5
  //   // rangeMax = Math.ceil(rangeMax / 0.5) * 0.5; // round to 0.5
  //   // rangeMin = Math.abs(Math.max(Math.floor(rangeMin / step) * step, colourMin));
  //   // rangeMax = Math.abs(Math.min(Math.ceil(rangeMax / step) * step, colourMax));

  //   // -2 to 3
  //   // -2, -1, 0, 1, 2, 3
  //   // ['#f4a582','#fddbc7','#f7f7f7','#d1e5f0','#92c5de','#4393c3']

  //   return colourRange.slice(centerIndex - (Math.abs(rangeMin) / step), centerIndex + (Math.abs(rangeMax) / step) + 1);
  // }

  function calibrateEnds(rangeMin, rangeMax, step = -1, colourMin = -5, colourMax = 5) {
    if (step > 0) {
      rangeMin = Math.max(Math.floor(rangeMin / step) * step, colourMin);
      rangeMax = Math.min(Math.ceil(rangeMax / step) * step, colourMax);
    } else {
      // 2.1 to -0.6 // 0.2 to -0.3
      const magMax = Math.abs(rangeMax) > Math.abs(rangeMin) ? rangeMax : rangeMin;
      const magMin = Math.abs(rangeMax) < Math.abs(rangeMin) ? rangeMax : rangeMin;
      // const absMax = Math.max(Math.abs(rangeMin), Math.abs(rangeMax))
      // const absMin = Math.min(Math.abs(rangeMin), Math.abs(rangeMax))
      // const maxStep = Math.max(Math.abs(colourMin), Math.abs(colourMax)); // 5
      const maxStep = 5; // Always using 11-step ranges atm

      step = Math.abs(magMax / maxStep); // 2.1 -> 0.42 // -0.3 -> 0.06
      const lowerVal = Math.sign(magMin) * (Math.ceil(Math.abs(magMin / step)) * step); // -0.6 -> -0.84 // 0.2 -> 0.24
      rangeMin = Math.min(lowerVal, rangeMin, 0); // -0.84, -0.6, 0 // 0.24, -0.3, 0
      rangeMax = Math.max(lowerVal, rangeMax, 0); // -0.84, 2.1, 0 // 0.24, 0.2, 0

      console.log(magMin, magMax, maxStep, step, lowerVal, rangeMin, rangeMax)
    }

    if (rangeMax > rangeMin) {
      return [rangeMin, rangeMax];  
    } else {
      return [rangeMax, rangeMin];  
    }
  }

  function selectColours([rangeMin, rangeMax], colourRange, step = -1, centerIndex = 5) {
    if (step > 0) {
      return colourRange.slice(centerIndex - (Math.abs(rangeMin) / step), centerIndex + (Math.abs(rangeMax) / step) + 1);
    } else {
      const magMax = Math.abs(rangeMax) > Math.abs(rangeMin) ? rangeMax : rangeMin;
      const magMin = Math.abs(rangeMax) < Math.abs(rangeMin) ? rangeMax : rangeMin;
      // const absMax = Math.max(Math.abs(rangeMin), Math.abs(rangeMax))
      // const absMin = Math.min(Math.abs(rangeMin), Math.abs(rangeMax))
      const maxStep = (colourRange.length - 1) / 2;

      const lowerIndex = Math.abs(Math.ceil(magMin / (magMax / maxStep)))
      console.log(magMin, magMax, maxStep, lowerIndex, Math.max(lowerIndex, 5), Math.min(lowerIndex, 5), colourRange.slice(centerIndex - Math.min(lowerIndex, 5), centerIndex + Math.max(lowerIndex, 5) + 1))

      // return colourRange.slice(centerIndex - Math.min(lowerIndex, 5), centerIndex + Math.max(lowerIndex, 5) + 1);
      if (magMax > 0) {
        return colourRange.slice(centerIndex - Math.min(lowerIndex, 5), centerIndex + Math.max(lowerIndex, 5) + 1);
      } else {
        return colourRange.slice(centerIndex - Math.min(lowerIndex, 5), centerIndex + Math.max(lowerIndex, 5) + 1).reverse();
      }
    }
  }

  // https://stackoverflow.com/questions/10756313/javascript-jquery-map-a-range-of-numbers-to-another-range-of-numbers#23202637
  function scaleRange (number, inMin, inMax, outMin, outMax) {
    return (number - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
  }

  return { colourMap, calibrateEnds, selectColours, scaleRange, calibratedEnds }
})
