import axios from "axios";

export default axios.create({
  // baseURL: "https://stabilitysort.org/api",
  baseURL: "http://localhost:8082",
  headers: {
    "Content-type": "application/json"
  }
});
