import { reactive } from "vue";

export const state = reactive({
  uniprotID: "",
  wildtypeSequence: "",
  selectedMutation: {
    aa_ref: null,
    aa_pos: null,
    aa_alt: null
  },
  updateAARef() {
    this.selectedMutation.aa_ref = this.wildtypeSequence[this.selectedMutation.aa_pos - 1]
  },
  wtViewport: {},
  calibratedEnds: {
    ddg: [],
    delta_score: [],
    score: []
  }
});
