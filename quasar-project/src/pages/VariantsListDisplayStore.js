import { reactive } from "vue";
import VariantDataService from "../../services/VariantDataService";

export const variantState = reactive({
  hash: "",
  hashReady: false,
  selectedVariant: {
    uniprot: null, 
    enst: null, 
    chr: null, 
    pos: null, 
    ref: null, 
    alt: null, 
    aa_pos: null, 
    aa_ref: null, 
    aa_alt: null, 
  },
  async refreshSelectedVariant({ hash, chr, pos, alt }) {
    console.log( hash, chr, pos, alt);
    hash = hash ? hash : this.hash;
    chr = chr ? chr: this.selectedVariant.chr;
    pos = pos ? pos: this.selectedVariant.pos;
    alt = alt ? alt: this.selectedVariant.alt;

    // const variantParams = { chr, pos, alt };
    // Object.keys(variantParams).forEach((k) => {
    //   variantParams[k] = variantParams[k] ? variantParams[k] : this.selectedVariant[k]
    // });

    // console.log(hash, variantParams);

    const response = await VariantDataService.retrieveVariantInfo(hash, Object.keys(this.selectedVariant).join(","), chr, pos, alt);

    console.log(response);

    // var responseData = response.data[0];
    // responseData.uniprotID = responseData.uniprot;

    response.data[0].uniprot = response.data[0].uniprot.split("-")[0].trim();

    this.selectedVariant = response.data[0];
  }
});
