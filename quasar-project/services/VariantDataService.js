import http from "../src/http-common";

class VariantDataService {
  getAll() {
    return http.get("/");
  }

  getByUniprot(id, select = "") {
    return http.get(`/${id}?select=${select}`);
  }

  downloadData() {
    window.open(`${http.defaults.baseURL}/data`, "_self");
    // return http.get(`/data`);
  }

  downloadCSQ(hash) {
    window.open(`${http.defaults.baseURL}/csq/${hash}`, "_self");
    // return http.get(`/data`);
  }

  uploadFile(file, onUploadProgress) {
    console.log(file);
    let formData = new FormData();
    formData.append("file", file);
    console.log(formData);
    return http.post("/upload", formData, {
      headers: {
        "Content-Type": "multipart/form-data"
        // "Content-Type": "application/octet-stream"
      }, 
      onUploadProgress
    });
  }

  retrieveCSV(hash, select = "", page, itemsPerPage, sortBy) {
    return http.post(`/csv/${hash}?select=${select}`, 
      { page, itemsPerPage, sortBy });
  }

  retrieveCSVHeaders(hash) {
    return http.get(`/csv/headers/${hash}`);
  }

  retrieveVariantFromCSV(hash, select = "", chr, pos, alt) {
    return http.get(`/csv/one/${hash}?select=${select}&chr=${chr}&pos=${pos}&alt=${alt}`);
  }

  retrieveDDGForUniprot(uniprotID, select = "") {
    return http.get(`/gene/variant/${uniprotID}?select=${select}`);
  }

  retrieveVariantInfo(hash, select, chr, pos, alt) {
    return http.get(`/variant/${hash}?select=${select}&chr=${chr}&pos=${pos}&alt=${alt}`);
  }

  retrieveProtein(id, select = "", page, itemsPerPage, sortBy) {
    return http.post(`/formatted/${id}?select=${select}`, 
      { page, itemsPerPage, sortBy });
  }

  retrieveProteinMutation(id, select = "", aa_pos, aa_alt) {
    return http.post(`/formattedOne/${id}?select=${select}`, 
      { aa_pos, aa_alt });
  }

  loadFile(fileName) {
    return http.get(`/load/${fileName}`);
  }
  // loadFile(fileName) {
  //   return resources.get(`/${fileName}`);
  // }

  retrieveUniprotDescription(id) {
    return http.get(`/description/${id}`);
  }

  getWtSequence(uniprotID) {
    return http.get(`/wildtypeSequence/${uniprotID}`);
  }

  getUniprotMap() {
    return http.get(`/uniprotMap`);
  }

  checkHash(hash) {
    return http.get(`/upload/${hash}`);
  }

  processHash(hash) {
    return http.get(`/process/${hash}`);
  }

  makeMutatedPDB(uniprotID, position, aminoAcid) {
    return http.post("/makeMutatedPDB", { uniprotID, position, aminoAcid });
  }
}

export default new VariantDataService();
