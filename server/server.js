#!/usr/bin/env node

const express = require("express");
const cors = require("cors");
const multer = require("multer");

const app = express();

var corsOptions = {
  origin: "https://stabilitysort.org:443"
  // origin: "http://localhost:9000"
};

app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(express.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));

// Parse file upload requests
app.use(multer({dest:"./uploads"}).any());

// require("./api/routes/protein.routes")(app); // app.use(routes);

const router = require("./api/routes/protein.routes");
app.use("/", router);

// set port, listen for requests
const PORT = process.env.PORT || 8081;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});

module.exports = app;
