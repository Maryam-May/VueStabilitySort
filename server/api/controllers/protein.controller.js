const util = require("util");
const { existsSync, rename, writeFile, unlink } = require("fs");
const exists = util.promisify(require("fs").exists);
const { execSync } = require("child_process");
const exec = util.promisify(require("child_process").exec);
const path = require("path");

const db = require("../models");

const DEBUG = true;
function debugLog() {
    if ( DEBUG ) {
        console.log.apply(this, arguments);
    }
}

exports.uploadFile = (req, res, next) => {
  const tmpPath = req.files[0].path;
  const size = req.files[0].size;
  const fileName = req.files[0].originalname;

  debugLog(`uploadFile. tmpPath: ${tmpPath}; size: ${size}; fileName: ${fileName};`);

  const hash = execSync(`md5sum ${tmpPath} | cut -d" " -f1`).toString().trim();
  debugLog(hash);
  debugLog(`uploadFile. hash: ${hash};`);
  rename(tmpPath, path.join(path.dirname(tmpPath), hash));
  // execSync(`mv ${tmpPath} ${path.join(path.dirname(tmpPath), hash)}`).toString().trim();

  // res.send(hash);
  next();
};

exports.checkHash = (req, res, next) => {
  const hash = req.params.hash;

  const csqFileName = `output/out.${hash}.csq.hom.tsv.gz`
  // const csvFileName = `output/out.${hash}.processed.csv`

  debugLog(`checkHash. hash: ${hash}; csqFileName: ${csqFileName};`);

  if (existsSync(`${csqFileName}.inprogress`)) {
    res.end();
  } else {
    res.setHeader("Content-Type", "text/plain");
    res.write("Processing...");
    next();
  }
}

exports.processHash = async (req, res) => {
  const hash = req.params.hash;

  const hashPath = path.join("uploads", hash);
  const csqFileName = `output/out.${hash}.csq.hom.tsv.gz`;
  // const csvFileName = `output/out.${hash}.processed.csv`

  debugLog(`processHash. hash: ${hash}; csqFileName: ${csqFileName};`);

  // if (! existsSync(csqFileName)) {
  if (! await exists(csqFileName)) {
      writeFile(`${csqFileName}.inprogress`, "", (err) => {
      if (err) throw err;
      debugLog(`${csqFileName}.inprogress was created.`);
    });
    await exec(`bash scripts/wrap_csq_b38.sh ${hashPath} ${csqFileName}`);
  }

  if (await exists(csqFileName) && await exists(`${csqFileName}.inprogress`)) {
    unlink(`${csqFileName}.inprogress`, (err) => {
      if (err) throw err;
      debugLog(`${csqFileName}.inprogress was deleted.`);
    })
  //   for (hashInputs in [hashPath, `${hashPath}.bgz`, `${hashPath}.tbi`]) {
  //     // rename(hashPath)
  //   }
  }

  // execSync(`Rscript --vanilla scripts/processCSQ.R ${csqFileName} ${csvFileName} resources/HUMAN_9606_idmapping.enst.tsv`);

  debugLog("processHash. Complete!");

  res.write("Complete!");
  res.end();
}

exports.retrieveCSVFromHash = (req, res) => {
  const hash = req.params.hash;

  const page = req.body.page;
  const itemsPerPage = req.body.itemsPerPage;
  const sortBy = req.body.sortBy; // [ { key: 'aa_ref', order: 'asc' } ] // order can also be 'desc'
  const start = (page - 1) * itemsPerPage;
  const end = start + itemsPerPage;

  var SQLSelect = req.query.select; // aa_ref,aa_pos,aa_alt,ddg
  if (!SQLSelect) {
    SQLSelect = "*"
  }

  var SQLQuery = `SELECT ${SQLSelect} FROM _${hash}_gno`;
  sortBy.length ? 
    SQLQuery += ` ORDER BY ${sortBy[0].key} ${sortBy[0].order}` : 
    SQLQuery += " ORDER BY chr, pos";
  // SQLQuery += ` LIMIT ${itemsPerPage} OFFSET ${(page - 1) * itemsPerPage}`;

  // debugLog(SQLQuery);

  debugLog(`retrieveCSVFromHash. hash: ${hash}; page: ${page}; itemsPerPage: ${itemsPerPage}; sortBy: ${sortBy}; SQLQuery: ${SQLQuery};`);

  db.con.all(SQLQuery,
    (err, data) => {
      if (err) {
        res.status(500).send({
          message:
            err.message || `retrieveCSVFromHash. Some error occurred while retrieving the proteins where uniprot is ${id}.`
        });
      };
      debugLog("retrieveCSVFromHash. Sending off data now.");
      // debugLog(data);
      res.send({
        items: data.slice(start, end),
        total: data.length
      })
    });
}

exports.importCSQ = async (req, res, next) => {
  const hash = req.params.hash;

  const csqFileName = `output/out.${hash}.csq.hom.tsv.gz`
  // const csvFileName = `output/out.${hash}.processed.csv`

  // const tableName = await loadCSVFileIntoDb(csvFileName, "precsq");
  // debugLog(tableName);

  debugLog(`importCSQ. hash: ${hash}; csqFileName: ${csqFileName}; ${req.url}`);

  var gnoExists = false;
  var csqExists = false;

  await new Promise((resolve, reject) => {
    db.con
    .all(`SELECT view_name FROM duckdb_views() WHERE view_name = '_${hash}_gno';`,
    (err, data) => {
      if (err) {
        debugLog(`importCSQ. Somehow failed while checking if _${hash}_gno exists?`, err);
        throw err;
        reject();
      }
      if (data.length > 0) {
        debugLog(`importCSQ. _${hash}_gno exists!`);
        gnoExists = true;
      } else {
        debugLog(`importCSQ. _${hash}_gno doesn't exist.`);
      }
      resolve();
    })
  })

  if (gnoExists) {
    return next();
  }

  await new Promise((resolve, reject) => {
    db.con
    .all(`SELECT view_name FROM duckdb_views() WHERE view_name = '_${hash}_csq';`,
    (err, data) => {
      if (err) {
        debugLog(`importCSQ. Somehow failed while checking if _${hash}_csq exists?`, err);
        throw err;
        reject();
      }
      if (data.length > 0) {
        debugLog(`importCSQ. _${hash}_csq exists!`);
        csqExists = true;
      } else {
        debugLog(`importCSQ. _${hash}_csq doesn't exist.`);
      }
      resolve();
    })
  })

  debugLog("importCSQ. Creating hash table (csq) now.");

  if (! csqExists) {
    await new Promise((resolve, reject) => {
      db.con
      .run(`
        CREATE OR REPLACE VIEW _${hash}_csq AS 
        FROM read_csv(
          "${csqFileName}", 
          delim = "\t", 
          columns = {
            "sample_id": "VARCHAR", 
            "phase": "VARCHAR", 
            "chr": "VARCHAR", 
            "pos": "INTEGER", 
            "gene": "VARCHAR", 
            "enst": "VARCHAR", 
            "region": "VARCHAR", 
            "strand": "VARCHAR", 
            "aa_ref": "VARCHAR", 
            "aa_pos": "INTEGER", 
            "aa_alt": "VARCHAR", 
            "ref": "VARCHAR", 
            "alt": "VARCHAR"
            }
          ) 
        LEFT JOIN enst 
        USING (
          enst
          );`,
      (err, res) => {
        if (err) {
          debugLog(`importCSQ. Could not load ${csqFileName}!`, err);
          throw err;
          reject();
        }
        debugLog(`importCSQ. Loaded ${csqFileName}!`);
        resolve();
      });
    });
  }

  debugLog("importCSQ. Creating hash table (gno) now.");

  await new Promise((resolve, reject) => {
    db.con
      .run(`
      CREATE OR REPLACE VIEW _${hash}_gno AS 
      SELECT
        CAST(ROW_NUMBER() OVER (ORDER BY amSsG312.chr, amSsG312.pos, amSsG312.alt) AS INTEGER) AS id, 
        * 
      FROM _${hash}_csq 
      LEFT JOIN amSsG312 
      USING (
        uniprot, 
        aa_pos, 
        aa_alt
        );
        `, (err, data) => {
        if (err) {
          res.status(500).send({
            message:
              err.message || `importCSQ. Some error occurred while producing the table gno for hash: '${hash}'.`
          });
          throw err;
          reject();
        };
        debugLog("importCSQ. Hash table (gno) created.");
        resolve();
      });
  });

  next();
}

exports.retrieveCSVHeadersFromHash = async (req, res) => {
  const hash = req.params.hash;

  debugLog(`retrieveCSVHeadersFromHash. Collecting headers now. ${req.url}`);

  db.con.all(`
    SELECT column_name, data_type
    FROM information_schema.columns
    WHERE table_name = '_${hash}_gno';
  `, (err, data) => {
  // db.con.all(`
  //   SELECT 
  //     alias(COLUMNS(*)), 
  //     typeof(COLUMNS(*)), 
  //     stats(COLUMNS(*)), 
  //     max(COLUMNS(*)) 
  //   FROM gno 
  //   GROUP BY ALL;
  // `, (err, data) => {
    if (err) {
      res.status(500).send({
        message:
          err.message || `retrieveCSVHeadersFromHash. Some error occurred while retrieving the headers for processed CSV of hash: '${hash}'.`
      });
      throw err;
    };
    debugLog("retrieveCSVHeadersFromHash. Sending off headers now.");
    // debugLog(data);
    res.send(data);
  });
};

exports.downloadData = (req, res) => {
  const rootPath = path.dirname(path.dirname(__dirname));
  const fileName = "stabilityPredictions.tar.gz";
  const filePath = path.join(rootPath, `resources/${fileName}`);

  debugLog(`downloadData. file: ${filePath}.`);
  res.download(filePath, fileName, (err) => {
    debugLog(`downloadData. headersSent: ${res.headersSent}.`);
    // if (err && ! res.headersSent) {
    if (err) {
      debugLog(err || `downloadData. Some error occurred while sending the file ${filePath}.`);
      // res.status(500).send({
      //   message:
      //     err.message || `downloadData. Some error occurred while sending the file ${filePath}.`
      // });
    }
  });
};

exports.downloadCSQ = (req, res) => {
  const hash = req.params.hash;
  const csqFileName = `out.${hash}.csq.hom.tsv.gz`;
  const rootPath = path.dirname(path.dirname(__dirname));
  const filePath = path.join(rootPath, `output/${csqFileName}`);

  debugLog(`downloadCSQ. file: ${filePath}.`);
  res.download(filePath, csqFileName, (err) => {
    debugLog(`downloadData. headersSent: ${res.headersSent}.`);
    // if (err && ! res.headersSent) {
    if (err) {
      debugLog(err || `downloadCSQ. Some error occurred while sending the file ${filePath}.`);
      // res.status(500).send({
      //   message:
      //     err.message || `downloadCSQ. Some error occurred while sending the file ${filePath}.`
      // });
    }
  });
};

exports.retrieveDDGForUniprot = (req, res) => {
  // const hash = req.params.hash;
  const uniprotID = req.params.uniprotID;

  var SQLSelect = req.query.select; // ddg
  if (!SQLSelect) {
    SQLSelect = "*"
  }

  var SQLQuery = `SELECT ${SQLSelect} FROM amSsG312 WHERE uniprot = '${uniprotID}'`;

  debugLog(`retrieveDDGForUniprot. uniprotID: ${uniprotID}; SQLQuery: ${SQLQuery};`);

  db.con.all(SQLQuery,
    (err, data) => {
      if (err) {
        res.status(500).send({
          message:
            err.message || `retrieveDDGForUniprot. Some error occurred while retrieving the DDG for uniprot ID ${uniprotID}.`
        });
      };
      debugLog(`retrieveDDGForUniprot. Sending now.`);
      res.send(data);
    });
};

exports.retrieveVariantInfo = (req, res) => {
  const hash = req.params.hash;
  const chr = req.query.chr;
  const pos = Number(req.query.pos);
  const alt = req.query.alt;

  var SQLSelect = req.query.select; // aa_ref,aa_pos,aa_alt,ddg
  if (!SQLSelect) {
    SQLSelect = "*"
  }

  var SQLQuery = `SELECT ${SQLSelect} FROM _${hash}_gno `;
  SQLQuery += ` WHERE chr = '${chr}'`;
  SQLQuery += ` AND pos = ${pos}`;
  SQLQuery += ` AND alt = '${alt}'`;

  debugLog(`retrieveVariantInfo. hash: ${hash}; chr: ${chr}; pos: ${pos}; alt: ${alt}; SQLQuery: ${SQLQuery};`);

  db.con.all(SQLQuery,
    (err, data) => {
      if (err) {
        res.status(500).send({
          message:
            err.message || `retrieveVariantInfo. Some error occurred while retrieving the variant info for (${chr}-${pos}-${alt}) for which the hash is ${hash}.`
        });
      };
      debugLog(`retrieveVariantInfo. Sending now.`);
      res.send(data);
    });
};

exports.loadFile = (req, res) => {
  const fileName = req.params.fileName;
  const rootPath = path.dirname(path.dirname(__dirname));

  const options = {
    root: path.join(rootPath, "resources/pdb")
  }

  debugLog(`loadFile. fileName: ${fileName}; rootPath: ${rootPath}; options: ${options};`);

  res.sendFile(fileName, options, (err) => {
    if (err) {
      // throw err;
      res.status(500).send({
        message:
          err || `loadFile. Could not retrieve ${fileName}.`
      });
      return;
    } else {
      debugLog("loadFile. Sent.");
    }
  });
};

// Retrieve all proteins from the database.
exports.findAll = (req, res) => {
  const SQLQuery = "SELECT * FROM amSsG312 LIMIT 10";

  debugLog(`findAll. SQLQuery: ${SQLQuery};`);

  db.con.all(SQLQuery,
    (err, data) => {
      if (err) {
        res.status(500).send({
          message:
            err.message || "findAll. Some error occurred while retrieving proteins."
        });
      };
      debugLog(`findAll. Sending now.`);
      res.send(data);
    });
};

exports.findOne = (req, res) => {
  const id = req.params.id;
  var SQLSelect = req.query.select; // aa_ref,aa_pos,aa_alt,ddg
  if (!SQLSelect) {
    SQLSelect = "*"
  }

  const SQLQuery = `SELECT ${SQLSelect} FROM amSsG312 WHERE uniprot = '${id}' ORDER BY aa_pos,aa_alt`;

  debugLog(`findOne. id: ${id}; SQLQuery: ${SQLQuery};`);

  db.con.all(SQLQuery,
    (err, data) => {
      if (err) {
        res.status(500).send({
          message:
            err.message || `findOne. Some error occurred while retrieving the proteins where uniprot is ${id}.`
        });
      };
      debugLog(`findOne. Sending now.`);
      res.send(data);
    });
};

exports.findOneFormatted = (req, res) => {
  const page = req.body.page;
  const itemsPerPage = req.body.itemsPerPage;
  const sortBy = req.body.sortBy; // [ { key: 'aa_ref', order: 'asc' } ] // order can also be 'desc'

  const start = (page - 1) * itemsPerPage;
  const end = start + itemsPerPage;

  const id = req.params.id;
  var SQLSelect = req.query.select; // aa_ref,aa_pos,aa_alt,ddg
  if (!SQLSelect) {
    SQLSelect = "*"
  }

  // db.con
  //   .run(`
  //     DROP TABLE IF EXISTS gnomad312_${id};
  //     CREATE TABLE gnomad312_${id} AS
  //     SELECT ${SQLSelect}
  //     FROM gnomad312
  //     WHERE uniprot = '${id}'
  //     ORDER BY pos;
  //   `);

  var SQLQuery = `SELECT ${SQLSelect} FROM gnomad312 WHERE uniprot = '${id}'`;
  sortBy.length ? 
    SQLQuery += ` ORDER BY ${sortBy[0].key} ${sortBy[0].order}` : 
    SQLQuery += " ORDER BY pos";
  // SQLQuery += ` LIMIT ${itemsPerPage} OFFSET ${(page - 1) * itemsPerPage}`;

  SQLQuery += ";";

  debugLog(`findOneFormatted. id: ${id}; page: ${page}; itemsPerPage: ${itemsPerPage}; sortBy: ${sortBy}; SQLQuery: ${SQLQuery};`);

  db.con.all(SQLQuery,
    (err, data) => {
      if (err) {
        res.status(500).send({
          message:
            err.message || `findOneFormatted. Some error occurred while retrieving the proteins where uniprot is ${id}.`
        });
      };
      debugLog("findOneFormatted. Sending now.");
      res.send({
        items: data.slice(start, end),
        total: data.length
      })
    });
};

exports.getUniprotDescription = (req, res) => {
  const id = req.params.id;

  var SQLQuery = `
    SELECT DISTINCT 
      gnomad312.desc, 
      enst.enst, 
      uniprotStats.Observed_mDoF_variants AS o_mDoF, 
      uniprotStats.Expected_mDoF_variants AS e_mDoF, 
      uniprotStats."Observed_GnomAD2.1.1_LoF_variants" AS e_g211_LoF, 
      CAST(uniprotStats."Expected_GnomAD2.1.1_LoF_variants" AS INTEGER) AS o_g211_LoF, 
      uniprotStats.Gene_CDS_length_nt AS cds_len, 
      uniprotStats.ProportionPotentialInstabiliySites_PPIS AS ppis, 
      CAST(uniprotStats.AlphaMissenseMedianPathScore AS INTEGER) AS am_mps, 
      uniprotStats."GnomAD2.1.1_LOEUFbin" AS g211_loeuf 
    FROM gnomad312 
    FULL JOIN enst 
    USING (uniprot) 
    FULL JOIN uniprotStats 
    ON enst.enst = uniprotStats.CanonicalTranscript 
    WHERE uniprot = '${id}'
    `;

  debugLog(`getUniprotDescription. id: ${id}; SQLQuery: ${SQLQuery};`);

  db.con.all(SQLQuery,
    (err, data) => {
      if (err) {
        res.status(500).send({
          message:
            err.message || `getUniprotDescription. Some error occurred while retrieving the proteins where uniprot is ${id}.`
        });
      };
      debugLog(`getUniprotDescription. Sending now.`);
      res.send(data);
    });
};

exports.retrieveVariantFromCSV = (req, res) => {
  const hash = req.params.hash;
  const chr = req.query.chr;
  const pos = Number(req.query.pos);
  const alt = req.query.alt;

  var SQLSelect = req.query.select; // aa_ref,aa_pos,aa_alt,ddg
  if (!SQLSelect) {
    SQLSelect = "*"
  }

  var SQLQuery = `SELECT ${SQLSelect} FROM _${hash}_gno `;
  SQLQuery += ` WHERE chr = '${chr}'`;
  SQLQuery += ` AND pos = ${pos}`;
  SQLQuery += ` AND alt = '${alt}'`;

  debugLog(`retrieveVariantFromCSV. hash: ${hash}; chr: ${chr}; pos: ${pos}; alt: ${alt}; SQLQuery: ${SQLQuery};`);

  db.con.all(SQLQuery,
    (err, data) => {
      if (err) {
        res.status(500).send({
          message:
            err.message || `retrieveVariantFromCSV. Some error occurred while retrieving the variant where hash is ${hash}.`
        });
      };
      debugLog("retrieveVariantFromCSV. Sending now.");
      res.send(data);
    });
};

exports.findOneMutFormatted = (req, res) => {
  const aa_pos = Number(req.body.aa_pos);
  const aa_alt = req.body.aa_alt;

  const id = req.params.id;
  var SQLSelect = req.query.select; // aa_ref,aa_pos,aa_alt,ddg
  if (!SQLSelect) {
    SQLSelect = "*"
  }

  var SQLQuery = `SELECT ${SQLSelect} FROM gnomad312 WHERE uniprot = '${id}'`;
  SQLQuery += ` AND aa_pos = ${aa_pos}`;
  SQLQuery += ` AND aa_alt = '${aa_alt}'`;

  debugLog(`findOneMutFormatted. id: ${id}; aa_pos: ${aa_pos}; aa_alt: ${aa_alt}; SQLQuery: ${SQLQuery};`);

  db.con.all(SQLQuery,
    (err, data) => {
      if (err) {
        res.status(500).send({
          message:
            err.message || `findOneMutFormatted. Some error occurred while retrieving the proteins where uniprot is ${id}.`
        });
      };
      debugLog("findOneMutFormatted. Sending now.");
      res.send(data);
    });
};

exports.getWtSequence = (req, res) => {
  const uniprotID = req.params.uniprotID;

  const SQLQuery = `SELECT DISTINCT sequence FROM protSeq WHERE uniprot = '${uniprotID}'`;

  debugLog(`getWtSequence. uniprotID: ${uniprotID}; SQLQuery: ${SQLQuery};`);

  db.con.all(SQLQuery,
    (err, data) => {
      if (err) {
        res.status(500).send({
          message:
            err.message || `getWtSequence. Some error occurred while retrieving the sequence for the protein with uniprot is ${id}.`
        });
      };
      debugLog("getWtSequence. Sending now.");
      res.send(data);
    });
};

exports.getUniprotMap = (req, res) => {
  const SQLQuery = `SELECT DISTINCT uniprot, SYMBOL FROM uniprotMap;`;

  debugLog(`getUniprotMap. SQLQuery: ${SQLQuery};`);

  db.con.all(SQLQuery,
    (err, data) => {
      if (err) {
        res.status(500).send({
          message:
            err.message || `getUniprotMap. Some error occurred while retrieving the uniprot to gene name table ${id}.`
        });
      };
      debugLog("getUniprotMap. Sending now.");
      res.send(data);
    });
};

exports.makeMutatedPDB = (req, res) => {
  const uniprotID = String(req.body.uniprotID);
  const position = String(req.body.position);
  const aminoAcid = String(req.body.aminoAcid);

  const mutatedPDBFileName = `AF-${uniprotID}-F1-model_v1_${position}${aminoAcid}.pdb`;

  debugLog(`makeMutatedPDB. uniprotID: ${uniprotID}; position: ${position}; aminoAcid: ${aminoAcid}; mutatedPDBFileName: ${mutatedPDBFileName};`);

  // const csqFileName = `output/out.${hash}.csq.hom.tsv.gz`
  if (! existsSync(mutatedPDBFileName)) {
    // execSync(`conda activate kidney; pymol -cq scripts/mutateSequence.py -- -i ../resources/pdb/AF-${uniprotID}-F1-model_v1.pdb -p ${position} -a ${aminoAcid} -o ../resources/pdb/mutated/${mutatedPDBFileName}`);
    execSync(`conda run -n pymol pymol -cq scripts/mutateSequence.py -- -i resources/pdb/AF-${uniprotID}-F1-model_v1.pdb -p ${position} -a ${aminoAcid} -o resources/pdb/mutated/${mutatedPDBFileName}`);
  }

  // res.send(mutatedPDBFileName);

  const rootPath = path.dirname(path.dirname(__dirname));
  // debugLog(path.join(rootPath, "resources/pdb"));

  const options = {
    root: path.join(rootPath, "resources/pdb/mutated")
  }

  debugLog(`makeMutatedPDB. mutatedPDBFileName: ${mutatedPDBFileName}; rootPath: ${rootPath}; options: ${options};`);

  res.sendFile(mutatedPDBFileName, options, (err) => {
    if (err) {
      // throw err;
      res.status(500).send({
        message:
          err || `makeMutatedPDB. Could not retrieve ${mutatedPDBFileName}.`
      });
      return;
    } else {
      debugLog("makeMutatedPDB. Sent.");
    }
  });
};
