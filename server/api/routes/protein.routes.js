// module.exports = app => {
const proteins = require("../controllers/protein.controller.js");

var router = require("express").Router();

router.get("/uniprotMap", proteins.getUniprotMap);

router.get("/data", proteins.downloadData);
router.get("/csq/:hash", proteins.downloadCSQ);

// router.param("check", (req, res, next, id) => {
//     // console.log(readdirSync("output"))
//     // console.log(readdirSync("uploads"))
//     // for (csqFileName in readdirSync("output")) {
//     //     console.log(csqFileName);
//     //     if (csqFileName.endsWith(".tbi")) {
//     //         continue
//     //     }
//     //     req.hash = csqFileName.replace("out.", "").replace(".csq.hom.tsv.gz", "");
//     //     next();
//     // }
//     req.hash = csqFileName.replace("out.", "").replace(".csq.hom.tsv.gz", "");
//     next();
// });
// router.get("/upload/check", proteins.processHash);
router.get("/upload/:hash", proteins.checkHash, proteins.processHash);
router.post("/upload", proteins.uploadFile, proteins.processHash);
router.post("/process/:hash", proteins.processHash);

router.get("/csv/headers/:hash", proteins.importCSQ, proteins.retrieveCSVHeadersFromHash);
router.get("/csv/one/:hash", proteins.importCSQ, proteins.retrieveVariantFromCSV);
router.post("/csv/:hash", proteins.importCSQ, proteins.retrieveCSVFromHash);

router.get("/gene/variant/:uniprotID", proteins.retrieveDDGForUniprot);

router.get("/variant/:hash", proteins.retrieveVariantInfo);

router.get("/load/:fileName", proteins.loadFile);

// Retrieve all mutations for one protein, formatted for GnomadTable.vue
router.post("/formatted/:id", proteins.findOneFormatted);

// Retrieve one mutation for a protein, formatted for GnomadTable.vue
router.post("/formattedOne/:id", proteins.findOneMutFormatted);

router.get("/description/:id", proteins.getUniprotDescription);

router.get("/wildtypeSequence/:uniprotID", proteins.getWtSequence);
router.post("/makeMutatedPDB", proteins.makeMutatedPDB);

router.get("/:id", proteins.findOne);
router.get("/", proteins.findAll);

module.exports = router;

//   app.use('/', router);
//   // app.use('/api', router);
// };
