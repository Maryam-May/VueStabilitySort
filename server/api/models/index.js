const dbConfig = require("../../config/db.config.js");

var duckdb = require("duckdb");
var ddb = new duckdb.Database(":memory:");
var con = ddb.connect();

var db = {};
db.con = con;

function connectDb(database, tableName, resourcePath) {
    database
      .run(`CREATE OR REPLACE VIEW ${tableName} AS FROM "${resourcePath}"`, 
        (err, res) => {
          if (err) {
            console.log(`Cannot connect to ${resourcePath}!`, err);
            throw err;
          }
          console.log(`Connected to ${resourcePath}!`);
      });
  }

connectDb(db.con, "amSsG312", dbConfig.amSsG312);
connectDb(db.con, "gnomad312", dbConfig.gnomad312);
connectDb(db.con, "gnomad312bg", dbConfig.gnomad312bg);
connectDb(db.con, "gnomad211lof", dbConfig.gnomad211lof);
connectDb(db.con, "uniprotMap", dbConfig.uniprotMap);
connectDb(db.con, "uniprotStats", dbConfig.uniprotStats);

// connectDb(db.con, "protSeq", db.protSeq)
db.con
  .run(`CREATE OR REPLACE VIEW protSeq AS FROM read_csv("${dbConfig.protSeq}", delim = "\t", columns = {"uniprot": "VARCHAR", "sequence": "VARCHAR"})`, 
  (err, res) => {
      if (err) {
      console.log(`Cannot connect to ${dbConfig.protSeq}!`, err);
      throw err;
      }
      console.log(`Connected to ${dbConfig.protSeq}!`);
  });

// connectDb(db.con, "enst", db.enst)
db.con
  .run(`CREATE OR REPLACE VIEW enst AS FROM read_csv("${dbConfig.enst}", delim = "\t", columns = {"uniprot": "VARCHAR", "enst": "VARCHAR"})`, 
  (err, res) => {
      if (err) {
      console.log(`Cannot connect to ${dbConfig.enst}!`, err);
      throw err;
      }
      console.log(`Connected to ${dbConfig.enst}!`);
  });

db.con
  .all("SELECT current_setting('access_mode'), current_setting('threads'), current_setting('max_memory');", 
  (err, data) => {
    if (err) {
      console.log("Could not receive current settings", err);
      throw err;
    }
    console.log("Current settings retrieved!");
    console.log(data[0]);
  });

module.exports = db;
