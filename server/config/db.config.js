module.exports = {
  gnomad312: "resources/gnomad312_missense.parquet",
  gnomad312bg: "resources/g312_background.tsv.gz",
  gnomad211lof: "resources/gnomad.v2.1.1.lof_metrics.by_gene.tsv.gz",
  enst: "resources/HUMAN_9606_idmapping.enst.tsv",
  uniprotMap: "resources/uniprot_map.tsv.gz",
  protSeq: "resources/proteinSequences.tsv",
  amSsG312: "resources/AM_SS_G312.parquet",
  uniprotStats: "resources/data_for_web_missenselof_louef_obs_exp_UNFILTERED_WITHPPIC_231019_2.tsv"
};
