#!/usr/bin/env node

const express = require("express");
require("express-async-errors");
const cors = require("cors");
const multer = require("multer");

const util = require("util");
const { readdir, unlink } = require("fs");
const exists = util.promisify(require("fs").exists);
const { exec } = require("child_process");
const path = require("path");

function debugLog() {
    if ( process.env.NODE_ENV == "development" ) {
        console.log.apply(this, arguments);
    }
}

const app = express();

var corsOptions = {
  origin: "http://localhost:9000"
};

app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(express.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));

// Parse file upload requests
app.use(multer({dest:"./uploads"}).any());

// require("./api/routes/protein.routes")(app); // app.use(routes);

const router = require("./api/routes/protein.routes");
app.use("/", router);

// set port, listen for requests
const PORT = process.env.PORT || 8082;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});

function checkUnfinishedHashes() {
  // debugLog(`checkUnfinishedHashes. ${readdir("./output")};`);
  debugLog(`checkUnfinishedHashes. Start.`);
  readdir("./output", (err, files) => {
    files.forEach((csqFileNameIP, index) => {
      if (csqFileNameIP.endsWith(".inprogress")) {
      // if (csqFileName.includes(".")) {
          // return;
          debugLog(`checkUnfinishedHashes. csqFileNameIP: ${csqFileNameIP};`);
          if (exists(csqFileNameIP.replace(".inprogress", ""))) {
            debugLog(`checkUnfinishedHashes. Deleting ${csqFileNameIP}.`);
            unlink(`./output/${csqFileNameIP}`, (err) => {
              if (err) throw err;
              debugLog(`${csqFileNameIP}.inprogress was deleted.`);
            })
            return;
          }
          const hash = csqFileNameIP.replace("out.", "").replace(".csq.hom.tsv.gz.inprogress", "");
          debugLog(`checkUnfinishedHashes. Processing ${hash}.`);
          // exec(`bash scripts/wrap_csq_b38.sh ${path.join("uploads", hash)} ${csqFileNameIP}`)
        }
    })
  })// {
    // readdir("./uploads", (err, files) => {
    //   files.forEach((csqFileName, index) => {
    //     // if (csqFileName.endsWith(".tbi")) {
    //     if (csqFileName.includes(".")) {
    //         return;
    //     }
    //     debugLog(`checkUnfinishedHashes. Start. ${csqFileName};`);
    //   })
    // })// {
      
    // if (csqFileName.endsWith(".tbi")) {
    //     continue;
    // }
    // hash = csqFileName.replace("out.", "").replace(".csq.hom.tsv.gz", "");
    // debugLog(`checkUnfinishedHashes. csqFileName: ${csqFileName}; hash: ${hash};`);
    // if (! existsSync(csqFileName)) {
    //   execSync(`bash scripts/wrap_csq_b38.sh ${path.join("uploads", hash)} ${csqFileName}`);
    // }  
  }
// }

setTimeout(checkUnfinishedHashes, 1000 * 2); // 1 minute
setInterval(checkUnfinishedHashes, 1000 * 60 * 60); // every hour

module.exports = app;
