import argparse
import os
# import pymol
from pymol import cmd

# pymol -cq mutateSequence.py -- -i ../resources/AF-A0N4Z3-F1-model_v1.pdb -p 3 -a N -o AF-A0N4Z3-F1-model_v1_3N.pdb

parser = argparse.ArgumentParser("Mutate PDB File",
    description = "Imports a PDB file, mutates it at the specified position for the desired amino acid, and exports a new PDB file.")

parser.add_argument("-i", "--inputPDBFilePath", 
    help = "The PDB file to be mutated.",
    required = True,
    type = str)
parser.add_argument("-o", "--outputPDBFilePath", 
    help = "The path to save the mutated PDB file.",
    required = True,
    type = str)
parser.add_argument("-p", "--position", 
    help = "The position to mutate (1-based).",
    required = True,
    type = int)
parser.add_argument("-a", "--aminoAcid", 
    help = "The amino acid to substitute.",
    required = True,
    type = str)

args = parser.parse_args()


inputPDBFilePath = os.path.abspath(args.inputPDBFilePath)
position = args.position
aminoAcid = args.aminoAcid

if not args.outputPDBFilePath:
    args.outputPDBFilePath = os.path.join(os.path.dirname(args.inputPDBFilePath), os.path.basename(args.inputPDBFilePath) + f"_{position}_{aminoAcid}.pdb")

if not "/" in args.outputPDBFilePath:
    args.outputPDBFilePath = os.path.join(os.path.dirname(args.inputPDBFilePath), args.outputPDBFilePath)

outputPDBFilePath = os.path.abspath(args.outputPDBFilePath)

# Three-letter codes MUST be capitalised
aminoAcidCode = {
    "A": "ALA",
    "R": "ARG",
    "N": "ASN",
    "D": "ASP",
    "B": "ASX",
    "C": "CYS",
    "Q": "GLN",
    "E": "GLU",
    "Z": "GLX",
    "G": "GLY",
    "H": "HIS",
    "I": "ILE",
    "L": "LEU",
    "K": "LYS",
    "M": "MET",
    "F": "PHE",
    "P": "PRO",
    "S": "SER",
    "T": "THR",
    "W": "TRP",
    "Y": "TYR",
    "V": "VAL"
}

if len(aminoAcid) == 1:
    aminoAcid = aminoAcidCode[aminoAcid]

print(inputPDBFilePath)
print(position)
print(aminoAcid)
print(outputPDBFilePath)

# os.chdir(os.path.dirname(inputPDBFilePath))
# cmd.load(os.path.basename(inputPDBFilePath), object = "test", state = 0, format = "pdb")

cmd.load(inputPDBFilePath, object='input', state = 1)

# PDB = cmd.get_names()[0]

cmd.wizard("mutagenesis")
cmd.do("refresh_wizard")

cmd.get_wizard().set_mode(aminoAcid)
cmd.get_wizard().do_select(f"{position}/")
cmd.frame(1) # first rotamer might be most probable?
cmd.get_wizard().apply()

# cmd.set_wizard("done")
# cmd.save(outputPDBFilePath, PDB)
# cmd.save(outputPDBFilePath, PDB, object='input')
cmd.save(outputPDBFilePath, selection='input', state = 1)

cmd.set_wizard("done")

# ProtChainResiList = []

# for PDB in PDBs:
#     print(PDB)

#     ###### Get the ID numbers of c-alpha (CA) atoms of all residues
#     CAindex = cmd.identify(f"{PDB} and name CA")
#     print(CAindex)

#     for CAID in CAindex:
#         #Using CA ID to get Chain and Residue information   
#         pdbstr = cmd.get_pdbstr(f"{PDB} and id {CAID}")
#         pdbstr_lines = pdbstr.splitlines()
#         print(pdbstr_lines)
#         chain = pdbstr_lines[0].split()[4]
#         resi = pdbstr_lines[0].split()[5]
#         ProtChainResiList.append([PDB,chain,resi])
#         #b.check the output of the list
#     for output in ProtChainResiList:
#         print (output)
#     #5.Divide PDBid,Chain, and Residue (p,c,r) present in list to do a proper selection for the PyMOL wizard.

# p, c, r = ProtChainResiList[position]
# #If you want to select a range of residues you will have to add above,for example, ProtChainResiList[0:100] to select the
# ##first residue and the 99th one.
# cmd.wizard("mutagenesis")

# print(p, c, r)

# cmd.refresh_wizard()

# #Selection to which type of residue you want to change
# cmd.get_wizard().set_mode(aminoAcid)
# ############################################################
# ##Possible mutation_type could be:##########################
# ##'VAL' for ALA TO VAL######################################
# ###'ALA' for any/not ALA to ALA#############################
# ###'GLY' for ALA to GLY#####################################
# ############################################################
# # #'selection' will select each residue that you have selected 
# # #on ProtChainResiList above using the PDBid,chain,and residue 
# # #present on your pdb file.If you didn't select a range on 
# # #ProteinChainResiList, it will do the mutation on all the residues
# #  #present in your protein.
# selection=f"/{p}//{c}/{r}"
# print(selection)
# #Selects where to place the mutation
# cmd.get_wizard().do_select(selection)
# ##Applies mutation
# cmd.get_wizard().apply()
# #Save each mutation and reinitialize the session before the next mutation
# ##to have pdb files only with the residue-specific single-point mutation you were
# ##interested.
# for PDB in PDBs:
#     cmd.set_wizard("done")
#     #Saving your mutated residue in a single pdb file. Use directory in
#     ##os.path.basename("(directory)%s"%selection)to add where you want your pdb file
#     ##to be saved. 
#     ##This will save the residue-specific mutation with the respective file name according to
#     ##mutation.
#     cmd.save((os.path.basename(selection)+"-mutation.pdb"),"%s"%PDB)
#     ##Reinitialize PyMOL to default settings.
#     cmd.reinitialize('everything')
#     #Load your original (non-mutated) PDB file.
#     # cmd.load(filename)

# def Mutagenesis(PDBFilePath, mutatedAminoAcid, mutationPosition, mutatedPDBFilePath):
#     cmd.load(PDBFilePath)

#     PDBs = cmd.get_names()

#     ProtChainResiList = []
#     for PDB in PDBs:

#         ###### Get the ID numbers of c-alpha (CA) atoms of all residues
#         CAindex = cmd.identify(f"{PDB} and name CA")
#         print(CAindex)

#         for CAID in CAindex:
#             #Using CA ID to get Chain and Residue information   
#             pdbstr = cmd.get_pdbstr(f"{PDB} and id {CAID}")
#             pdbstr_lines = pdbstr.splitlines()
#             chain = pdbstr_lines[0].split()[4]
#             resi = pdbstr_lines[0].split()[5]
#             ProtChainResiList.append([PDB,chain,resi])
#             #b.check the output of the list
#         for output in ProtChainResiList:
#             print (output)
#      #5.Divide PDBid,Chain, and Residue (p,c,r) present in list to do a proper selection for the PyMOL wizard.
#     for p, c, r in ProtChainResiList[mutationPosition]:
#         #If you want to select a range of residues you will have to add above,for example, ProtChainResiList[0:100] to select the
#         ##first residue and the 99th one.
#         cmd.wizard("mutagenesis")

#         print(p, c, r)

#         cmd.refresh_wizard()

#         #Selection to which type of residue you want to change
#         cmd.get_wizard().set_mode(mutatedAminoAcid)
#         ############################################################
#         ##Possible mutation_type could be:##########################
#         ##'VAL' for ALA TO VAL######################################
#         ###'ALA' for any/not ALA to ALA#############################
#         ###'GLY' for ALA to GLY#####################################
#         ############################################################
#         # #'selection' will select each residue that you have selected 
#         # #on ProtChainResiList above using the PDBid,chain,and residue 
#         # #present on your pdb file.If you didn't select a range on 
#         # #ProteinChainResiList, it will do the mutation on all the residues
#         #  #present in your protein.
#         # selection="/%s//%s/%s"%(p,c,r)
#         selection=f"/{p}//{c}/{r}"
#        #Print selection to check the output
#         print(selection)
#         #Selects where to place the mutation
#         cmd.get_wizard().do_select(selection)
#         ##Applies mutation
#         cmd.get_wizard().apply()
#         #Save each mutation and reinitialize the session before the next mutation
#         ##to have pdb files only with the residue-specific single-point mutation you were
#         ##interested.
#         for PDB in PDBs:
#             cmd.set_wizard("done")
#            #Saving your mutated residue in a single pdb file. Use directory in
#            ##os.path.basename("(directory)%s"%selection)to add where you want your pdb file
#            ##to be saved. 
#            ##This will save the residue-specific mutation with the respective file name according to
#            ##mutation.
#             cmd.save((os.path.basename("%s"%selection)+"-mutation.pdb"),"%s"%PDB)
#            ##Reinitialize PyMOL to default settings.
#             cmd.reinitialize('everything')
#             #Load your original (non-mutated) PDB file.
#             cmd.load(filename)

# Mutagenesis(inputPDBFilePath, aminoAcidCode[aminoAcid], position, outputPDBFilePath)