#!/bin/bash
# set -eo pipefail
set -x
GENOME=resources/Homo_sapiens.GRCh38.dna.primary_assembly.fa.gz
GFF3=resources/Homo_sapiens.GRCh38.107.chr.gff3.gz
ENST=resources/enst.canonical.txt
THREADS=8
VCF_FILE=$1
OUT_FILE=$2
REGION=$3
ONLY=""
if [[ $# -gt 2 ]]; then
	ONLY="-r $REGION"
fi
if [[ ! -e $OUT_FILE ]]; then
	# mimetype=$(file -b --mime-type "$1")

	# if [[ $mimetype == "application/gzip" || $mimetype == "application/x-gzip" ]]; then
	# 	# if ! tabix -f $VCF_FILE; then
	# 	# 	# not bgzipped
	# 	# 	VCF_FILE=${VCF_FILE%.bgz}
	# 	# 	VCF_FILE=${VCF_FILE%.gz}
	# 	# 	sed -e 's/^chr//;/^$/d' $VCF_FILE | bgzip --threads $THREADS -c > $VCF_FILE.bgz
	# 	# fi
	# else

	# fi

	# if [[ ${VCF_FILE: -4} = '.bgz' ]]; then
	# 	mv $VCF_FILE ${VCF_FILE%.bgz}
	# 	VCF_FILE=${VCF_FILE%.bgz}
	# elif [[ ${VCF_FILE: -3} = '.gz' ]]; then
	# 	mv $VCF_FILE ${VCF_FILE%.gz}
	# 	VCF_FILE=${VCF_FILE%.gz}
	# fi

	# if [[ $(head -c 4 $VCF_FILE | xxd -p) = '1f8b0804' ]]; then # bgzip
	# 	mv $VCF_FILE $VCF_FILE.bgz
	# 	bgzip -d --threads $THREADS -c $VCF_FILE.bgz > $VCF_FILE
	# elif [[ $(head -c 4 $VCF_FILE | xxd -p) = '1f8b0808' ]]; then # gzip
	# 	mv $VCF_FILE $VCF_FILE.gz
	# 	gunzip -c $VCF_FILE.gz > $VCF_FILE
	# fi

	# # $VCF_FILE should now be unzipped

	# sed -e 's/^chr//;/^$/d' $VCF_FILE | bgzip --threads $THREADS -c > $VCF_FILE.bgz

	# # $VCF_FILE should now be bgzipped and formatted right (may still contain missense variants)

	# touch $OUT_FILE.inprogress

	if [[ ! -e $VCF_FILE.bgz ]]; then
		if [[ $(head -c 4 $VCF_FILE | xxd -p) = '1f8b0804' ]]; then # bgzip
			mv $VCF_FILE $VCF_FILE.bgz
			if zcat $VCF_FILE.bgz | head -n 1000 | grep -q '^chr'; then
				zcat $VCF_FILE.bgz | sed -e 's/^chr//;/^$/d' | bgzip --threads $THREADS -c > $VCF_FILE.bgz.tmp
				mv $VCF_FILE.bgz.tmp $VCF_FILE.bgz
			fi
		elif [[ $(head -c 4 $VCF_FILE | xxd -p) = '1f8b0808' ]]; then # gzip
			mv $VCF_FILE $VCF_FILE.gz
			gunzip -c $VCF_FILE.gz > $VCF_FILE
			sed -e 's/^chr//;/^$/d' $VCF_FILE | bgzip --threads $THREADS -c > $VCF_FILE.bgz
		else
			sed -e 's/^chr//;/^$/d' $VCF_FILE | bgzip --threads $THREADS -c > $VCF_FILE.bgz
		fi
	fi

	# $VCF_FILE should now be bgzipped and formatted right (may still contain missense variants)

	# if ! tabix -f $VCF_FILE; then
	# 	# not bgzipped
	# 	sed -e 's/^chr//;/^$/d' $VCF_FILE | bgzip --threads $THREADS -c > $VCF_FILE.bgz
	# fi

	# if [[ ${VCF_FILE: -4} != '.bgz' && ${VCF_FILE: -3} != '.gz' && $mimetype != "application/gzip" && $mimetype != "application/x-gzip" ]]; then
	# 	sed -e 's/^chr//;/^$/d' $VCF_FILE | bgzip --threads $THREADS -c > ${VCF_FILE}.gz
	# 	VCF_FILE=${VCF_FILE}.gz
	# elif zcat $VCF_FILE | head -n 1000 | grep -q '^chr'; then
	# 	zcat $VCF_FILE | sed -e 's/^chr//;/^$/d' | bgzip --threads $THREADS -c > ${VCF_FILE}.tmp
	# 	mv ${VCF_FILE}.tmp $VCF_FILE
	# fi
	# if [[ ${VCF_FILE: -4} == '.bgz' && ! -f ${VCF_FILE:: -4}.gz ]]; then
	# 	ln -s "../$VCF_FILE" "${VCF_FILE:: -4}.gz"
	# 	VCF_FILE=${VCF_FILE:: -4}.gz
	# fi

	if [[ ! -e $VCF_FILE.bgz.csi ]]; then
		bcftools index --threads $THREADS $VCF_FILE.bgz -o $VCF_FILE.bgz.csi
	fi
	if [[ ! -e $VCF_FILE.bgz.tbi ]]; then
		bcftools index --threads $THREADS -t $VCF_FILE.bgz -o $VCF_FILE.bgz.tbi
	fi

	# CMD="bcftools csq $ONLY --threads $THREADS -f $GENOME -g $GFF3 $VCF_FILE.bgz -p a -O t | grep missense | cut -f2-6 | tr '|' '\t' | grep -f $ENST | bgzip -c --threads $THREADS > $OUT_FILE"
	CMD="bcftools csq $ONLY --threads $THREADS -f $GENOME -g $GFF3 $VCF_FILE.bgz -p a -O t | grep -e '\Wmissense|' | cut -f2-6 | tr '|' '\t' | grep -f $ENST | scripts/splitHGVShom.pl | pigz -c > $OUT_FILE"
	echo >&2 "$CMD"
	eval "$CMD"

	# Check if there are non-missense mutations in the output file:

	mv $OUT_FILE ${OUT_FILE%.gz}.all.gz
	# OUT_FILE_ALL_LEN=$(zcat ${OUT_FILE%.gz}.all.gz | wc -l)

	# zgrep -E -e '^(\S+\s+){12}\S$' ${OUT_FILE%.gz}.all.gz | pigz -c > $OUT_FILE # 13 fields, no empty fields
	# zgrep -v -E -e '^(\S+\s+){12}\S$' ${OUT_FILE%.gz}.all.gz | pigz -c > ${OUT_FILE%.gz}.nonmis.gz
	# rm ${OUT_FILE%.gz}.all.gz

	zcat ${OUT_FILE%.gz}.all.gz | sed -n -E "/^(\S+\s+){12}\S$/{p;b} ; w ${OUT_FILE%.gz}.nonmis" | pigz -c > ${OUT_FILE%.gz}.gz
	pigz ${OUT_FILE%.gz}.nonmis
	rm ${OUT_FILE%.gz}.all.gz

	# rm $OUT_FILE.inprogress

	if [[ $(zcat ${OUT_FILE%.gz}.nonmis.gz | wc -l) -gt 0 ]]; then
		exit 100 # Non-missense variants present in original file but have been removed; warn user
	else
		rm ${OUT_FILE%.gz}.nonmis.gz
		exit 0
	fi

	# if [[ ! -e $OUT_FILE.csi ]]; then
	# 	bcftools index --threads $THREADS $OUT_FILE -o $OUT_FILE.csi
	# fi
	# if [[ ! -e $OUT_FILE.tbi ]]; then
	# 	bcftools index --threads $THREADS -t $OUT_FILE -o $OUT_FILE.tbi
	# fi
fi
