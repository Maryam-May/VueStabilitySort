library(data.table)

args = commandArgs(trailingOnly=TRUE)

if (length(args)!=3) {
  stop("Three arguments must be supplied (CSQ file, CSQ outfile, and ENST file, in this order).", call.=FALSE)
}

csq.file = args[1]
csq.outfile = args[2]
enst.file = args[3]

enst<-fread(enst.file,col.names=c('uniprot','enst'))

csq_head<-c('sample_id','phase','chr','pos','gene','enst','region','strand','aa_ref','aa_pos','aa_alt','ref','alt')
csq<-fread(csq.file,header=FALSE,col.names=csq_head,fill=TRUE) #final output file created by the bash script, fill=T needed for unknown genes
by_list<-names(csq)[c(1,3:6)]
csq[,zygosity:=fifelse(length(paste0(phase))>1,'Hom','Het'),by=c(by_list)]
csq[,phase:=NULL]
csq[,gene:=NULL]
csq<-unique(enst[csq,on='enst'])
order<-c('chr','pos','ref','alt','uniprot','enst','aa_ref','aa_pos','aa_alt','sample_id','zygosity','strand','region')
setcolorder(csq,order)
setorder(csq,chr,pos,uniprot,enst,sample_id)
csq<-na.omit(csq,c('uniprot'))
csq[,c("uniprot1","uniprot2"):=tstrsplit(uniprot,"-",fixed=TRUE)]
csq[,uniprot:=uniprot1]
csq[,c("uniprot1","uniprot2"):=NULL]
csq<-unique(csq[,c('enst','sample_id'):=list(toString(unique(sort(enst))),toString(unique(sort(sample_id)))),by=c('chr','pos','aa_ref','aa_pos','aa_alt','zygosity')])
fwrite(csq, csq.outfile)
