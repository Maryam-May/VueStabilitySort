#!/bin/bash
# set -eo pipefail
set -x
GENOME=resources/Homo_sapiens.GRCh38.dna.primary_assembly.fa.gz
GFF3=resources/Homo_sapiens.GRCh38.107.chr.gff3.gz
ENST=resources/enst.canonical.txt
THREADS=8
VCF_FILE=$1
OUT_FILE=$2
REGION=$3
ONLY=""
if [[ $# -gt 2 ]]; then
	ONLY="-r $REGION"
fi
if [[ ! -e $OUT_FILE ]]; then
	mimetype=$(file -b --mime-type "$1")
	if [[ ${VCF_FILE: -4} != '.bgz' && ${VCF_FILE: -3} != '.gz' && $mimetype != "application/gzip" && $mimetype != "application/x-gzip" ]]; then
		sed -e 's/^chr//;/^$/d' $VCF_FILE | bgzip --threads $THREADS -c > ${VCF_FILE}.gz
		VCF_FILE=${VCF_FILE}.gz
	elif zcat $VCF_FILE | head -n 1000 | grep -q '^chr'; then
		zcat $VCF_FILE | sed -e 's/^chr//;/^$/d' | bgzip --threads $THREADS -c > ${VCF_FILE}.tmp
		mv ${VCF_FILE}.tmp $VCF_FILE
	fi
	if [[ ${VCF_FILE: -4} == '.bgz' && ! -f ${VCF_FILE:: -4}.gz ]]; then
		ln -s "../$VCF_FILE" "${VCF_FILE:: -4}.gz"
		VCF_FILE=${VCF_FILE:: -4}.gz
	fi
	if [[ ! -e ${VCF_FILE}.csi && ! -e ${VCF_FILE}.tbi ]]; then
		bcftools index --threads $THREADS $VCF_FILE -o ${OUT_FILE}.csi
		bcftools index --threads $THREADS -t $VCF_FILE -o ${OUT_FILE}.tbi
	fi
	tabix $VCF_FILE
	CMD="bcftools csq $ONLY --threads $THREADS -f $GENOME -g $GFF3 $VCF_FILE -p a -O t | grep missense | cut -f2-6 | tr '|' '\t' | grep -f $ENST | pigz -c > $OUT_FILE"
	# CMD="bcftools csq $ONLY --threads $THREADS -f $GENOME -g $GFF3 $VCF_FILE -p a -O t | grep missense | cut -f2-6 | tr '|' '\t' | grep -f $ENST | scripts/splitHGVShom.pl | pigz -c > $OUT_FILE"
	echo >&2 "$CMD"
	eval "$CMD"
fi
